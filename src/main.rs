use std::fs;
use chrono::prelude::*;
use postmill::Client;
use itertools::Itertools;

fn main() {
    let slurs = [
        ("bitch", "making a fuss, whining, nagging"),
        ("cripple", "frozen by, stopped by, completely stuck, totally halted all operations (if using metaphors); physically disabled person, person with a mobility impairment, paralyzed person (if referring to a disabled person)"),
        ("daft", "dense, ignorant, lacks understanding, impulsive, risk-taker, uninformed, silly, foolish"),
        ("deranged", "dense, ignorant, lacks understanding, impulsive, risk-taker, uninformed, silly, foolish"),
        ("derp", "obviously, of course, uh yeah, ummm, ummm uhhh, um yeah, hell yeah, fuck yeah"),
        ("dumb", "dense, ignorant, lacks understanding, impulsive, risk-taker, uninformed, silly, foolish (to replace metaphor); nonspeaking, nonverbal, person with a speech impairment, person with a cognitive disability, Deaf person, hard of hearing person (to refer to a Deaf or disabled person)"),
        ("harelip", "cleft lip, cleft palate, cleft lip and palate"),
        ("fatso", "person of size"),
        ("fag", "cigarette, weak, powerless, hesitant"),
        ("faggot", "weak, powerless, hesitant"),
        ("idiot", "uninformed, reckless, impulsive, ignorant, risk-taking, risky and dangerous, dipshit"),
        ("imbecile", "uninformed, reckless, impulsive, ignorant, risk-taking, risky and dangerous, dipshit"),
        ("insan", "wild, confusing, unpredictable, impulsive, reckless, fearless, lives on the edge, thrill-seeker, risk-taker, out of control"),
        (" lame", "boring, uninteresting, monotonous, lacks excitement, uncool, out of fashion (if using metaphors); physically disabled person, person with a mobility impairment, paralyzed person (if referring to a disabled person), bad, gross, the pits"),
        ("loony", "wild, confusing, unpredictable, impulsive, reckless, fearless, lives on the edge, thrill-seeker, risk-taker, out of control"),
        ("lunatic", "wild, confusing, unpredictable, impulsive, reckless, fearless, lives on the edge, thrill-seeker, risk-taker, out of control, scary"),
        ("maniac", "wild, confusing, unpredictable, impulsive, reckless, fearless, lives on the edge, thrill-seeker, risk-taker, out of control, extremely energetic"),
        ("moron", "uninformed, reckless, impulsive, ignorant, risk-taking, risky and dangerous, dipshit"),
        ("pussy", "weak, powerless, hesitant"),
        ("retard", "uninformed, reckless, impulsive, ignorant, risk-taking, risky and dangerous, dipshit"),
        ("spaz", "klutzy, clumsy, forgetful, impulsive, reckless"),
        ("stupid", "uninformed, reckless, impulsive, ignorant, risk-taking, risky and dangerous, dipshit"),
        ("tranny", "I don't deserve to live")
    ];

    // Try to see if there is a post earlier than today
    let last_time = match fs::read_to_string("last_time.txt") {
        Ok(last) => DateTime::parse_from_rfc3339(&last.lines().next().unwrap()).unwrap(),
        Err(_) => {
            fs::write("last_time.txt", Utc::now().to_rfc3339()).unwrap();
            println!("No previous time found, writing new time!");
            return;
        }
    };

    let mut client = Client::new("https://raddle.me").unwrap();

    let mut login = false;

    // Get all the newest comments
    client.comments_until(last_time).unwrap()
        // Turn the vector into an iterator
        .iter()
        // Order the comments by last one first
        .rev()
        // Get the body text as lowercase
        .map(|comment| (comment.body.to_lowercase(), comment))
        // Check if the body text contains slur by making a big list of all the comments in
        // combination with all the slurs
        .cartesian_product(slurs.iter())
        // Then only keep the comments with a slur
        .filter(|((lowercase, _), (slur, _))| lowercase.contains(slur) && !lowercase.contains(&*format!("\"{}\"", slur)))
        // Finally post a comment
        .for_each(|((_, comment), (slur, alternative))| {
            println!("Comment {}", comment.id);

            fs::write("last_time.txt", comment.date.to_rfc3339()).unwrap();

            // Login
            if !login {
                client.login("LanguageBot", include_str!("../password.txt").trim()).unwrap();
                login = true;
            }

            // Send a reply comment
            let msg = format!("I've noticed you used the ableist word [\"{}\"](https://www.autistichoya.com/p/ableist-words-and-terms-to-avoid.html) in your comment, please refrain from doing so.  \n_Consider instead: {}_\n###### _Discussion about the words [here](https://raddle.me/f/Ability/55653/languagebot-list-of-ableist-words), stop this message from being posted by quoting the word: \"{}\"_", slur, alternative, slur);
            client.reply_comment(comment.submission_id, comment.id, &*msg).unwrap();
        });
}
